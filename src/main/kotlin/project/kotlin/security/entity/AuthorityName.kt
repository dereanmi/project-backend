package project.kotlin.security.entity


enum class AuthorityName{
    ROLE_CUSTOMER, ROLE_ADMIN, ROLE_GENERAL
}