package project.kotlin.security.contoller

data class JwtAuthenticationResponse(
        var token: String? = null
)