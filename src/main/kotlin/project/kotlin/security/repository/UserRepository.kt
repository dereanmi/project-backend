package project.kotlin.security.repository

import org.springframework.data.repository.CrudRepository
import project.kotlin.security.entity.JwtUser

interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}