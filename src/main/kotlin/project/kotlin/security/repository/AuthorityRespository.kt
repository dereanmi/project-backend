package project.kotlin.security.repository

import org.springframework.data.repository.CrudRepository
import project.kotlin.security.entity.Authority
import project.kotlin.security.entity.AuthorityName

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}