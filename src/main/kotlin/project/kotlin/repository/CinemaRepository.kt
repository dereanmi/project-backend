package project.kotlin.repository

import org.springframework.data.repository.CrudRepository
import project.kotlin.entity.Cinema

interface CinemaRepository : CrudRepository<Cinema, Long> {
    fun findBynameContainingIgnoreCase(name: String): List<Cinema>
}