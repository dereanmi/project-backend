package project.kotlin.repository

import org.springframework.data.repository.CrudRepository
import project.kotlin.entity.Movie

interface MovieRepository: CrudRepository<Movie, Long>{
    fun findBymovieNameContainingIgnoreCase(name:String): List<Movie>
}