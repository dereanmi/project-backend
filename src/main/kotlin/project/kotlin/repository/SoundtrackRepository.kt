package project.kotlin.repository

import org.springframework.data.repository.CrudRepository
import project.kotlin.entity.Soundtrack

interface SoundtrackRepository: CrudRepository<Soundtrack, Long>