package project.kotlin.repository

import org.springframework.data.repository.CrudRepository
import project.kotlin.entity.SelectedSeat

interface SelectedSeatRepository : CrudRepository<SelectedSeat, Long> {
//    fun findByverticalContainingIgnoreCase(vertical: String): List<SelectedSeat>
//    fun findByhorizontalContainingIgnoreCase(horizontal:String): List<SelectedSeat>
    //fun findByverticalContainingIgnoreCaseOrhorizontalContainingIgnoreCase(vertical: String,horizontal: String?): List<SelectedSeat>
}