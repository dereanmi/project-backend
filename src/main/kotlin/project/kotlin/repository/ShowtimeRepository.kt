package project.kotlin.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import project.kotlin.entity.Showtime

interface ShowtimeRepository : CrudRepository<Showtime, Long>{
    fun findBycinema_nameContainingIgnoreCase(name:String): List<Showtime>
    fun findBymovies_MovieNameContainingIgnoreCase(name: String): List<Showtime>
    //fun findAll(page:Pageable): Page<Showtime>
}