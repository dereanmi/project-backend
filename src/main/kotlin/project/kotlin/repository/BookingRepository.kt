package project.kotlin.repository

import org.springframework.data.repository.CrudRepository
import project.kotlin.entity.Booking

interface BookingRepository : CrudRepository<Booking, Long> {

}
