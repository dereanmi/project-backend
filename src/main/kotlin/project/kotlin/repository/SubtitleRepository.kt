package project.kotlin.repository

import org.springframework.data.repository.CrudRepository
import project.kotlin.entity.Subtitle

interface SubtitleRepository : CrudRepository<Subtitle, Long>