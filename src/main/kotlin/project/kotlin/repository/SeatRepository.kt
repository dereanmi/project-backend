package project.kotlin.repository

import org.springframework.data.repository.CrudRepository
import project.kotlin.entity.Seat

interface SeatRepository : CrudRepository<Seat, Long>