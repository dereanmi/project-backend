package project.kotlin.repository

import org.springframework.data.repository.CrudRepository
import project.kotlin.entity.SeatInShowTime

interface SeatInShowTimeRepository : CrudRepository<SeatInShowTime, Long> {

}