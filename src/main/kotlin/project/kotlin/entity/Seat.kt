package project.kotlin.entity

import javax.persistence.*

@Entity
data class Seat(var name:String? = null,
                var seatType: SeatType? = null,
                var price:Double? = null,
                @Column(name = "Seat_rows")
                var rows: Int? = null,
                @Column(name = "Seat_columns")
                var columns: Int? = null){

    @Id
    @GeneratedValue
    var id: Long? = null

}