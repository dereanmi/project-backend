package project.kotlin.entity

import javax.persistence.*

@Entity
data class Movie(var movieName: String? = null,
                 var duration: Int? = null,
                 var imageUrl: String? = null){
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToMany
    var soundtrack = mutableListOf<Soundtrack>()

    @ManyToMany
    var subtitle = mutableListOf<Subtitle>()

}

