package project.kotlin.entity

import java.util.*
import javax.persistence.*


@Entity
data class Booking(var showTimeId: Long? = null,
                   var createdDateTime: Long? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToMany
    var seats = mutableListOf<SeatInShowTime>()
}

