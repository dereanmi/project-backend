package project.kotlin.entity


import java.util.*
import javax.persistence.*

@Entity
data class Showtime(var Startdate: Long? = null,
                    var EndDate: Long? = null) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var movies: Movie? = null

    @OneToOne
    var cinema: Cinema? = null

    @OneToMany
    var selectedSeats = mutableListOf<SelectedSeat>()



}
