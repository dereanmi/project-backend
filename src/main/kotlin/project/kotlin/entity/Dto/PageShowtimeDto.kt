package project.kotlin.entity.Dto

import project.kotlin.entity.Showtime

data class PageShowtimeDto(var totalPages:Int? = null,
                           var totalElements:Long? = null,
                           var showtimes: List<Showtime> = mutableListOf())