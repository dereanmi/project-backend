package project.kotlin.entity.Dto

import java.util.*

data class ShowtimeDto (var Startdate: Long? = null,
                        var EndDate: Long? = null)