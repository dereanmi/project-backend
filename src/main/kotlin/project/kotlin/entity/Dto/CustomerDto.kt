package project.kotlin.entity.Dto

data class CustomerDto(var userName: String? = null,
                       var email: String? = null,
                       var password: String? = null,
                       var firstName: String? = null,
                       var lastName: String? =null,
                       var contact: String? = null,
                       var imageUrl: String? = null,
                       var dabitCart: String? = null,
                       var craditCart: String? = null,
                       var id:Long? = null

)

data class CustomerImageDto(
        var image:String?=null,
        var id:Long?=null
)
