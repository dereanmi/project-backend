package project.kotlin.entity.Dto

data class SelectedSeatDto(var row: String? = null,
                           var column: String? = null,
                           var status: Boolean? = null)