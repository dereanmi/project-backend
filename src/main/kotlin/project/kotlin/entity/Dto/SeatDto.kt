package project.kotlin.entity.Dto

data class SeatDto (var name:String? = null,
                    var seatType:String? = null,
                    var price:Double? = null,
                    var row:String? = null,
                    var column:String? = null)