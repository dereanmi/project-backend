package project.kotlin.entity.Dto

import java.util.*

data class BookingDto(var date: Date)