package project.kotlin.entity.Dto

data class MovieDto(var movieName: String? = null,
                    var duration: Int? = null,
                    var imageUrl: String? = null)