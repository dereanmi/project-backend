package project.kotlin.entity.Dto

class UserDto (var userName: String? = null,
                    var email: String? = null,
                    var password: String? = null,
                    var firstName: String? = null,
                    var lastName: String? =null,
                    var contact: String? = null,
                    var imageUrl: String? = null,
                    var dabitCart: String? = null,
                    var craditCart: String? = null,
               var authorities: List<AuthorityDto> = mutableListOf())