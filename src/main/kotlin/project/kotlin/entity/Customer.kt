package project.kotlin.entity

import javax.persistence.*

@Entity
data class Customer(
        override var userName: String? = null,
        override var email: String? = null,
        override var password: String? = null,
        override var firstName: String? = null,
        override var lastName: String? = null,
        override var contact: String? = null,
        var imageUrl: String? = null,
        var dabitCart: String? = null,
        var craditCart: String? = null

) : User(userName, email, password, firstName, lastName, contact){
//    @Id
//    @GeneratedValue
//    var id: Long? = null
}



