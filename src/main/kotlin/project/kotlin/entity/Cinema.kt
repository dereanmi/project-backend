package project.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany

@Entity
data class Cinema(var name: String? = null){

    @Id
    @GeneratedValue
    var id:Long? = null

    @ManyToMany
    var seat = mutableListOf<Seat>()

}
