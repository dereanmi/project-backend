package project.kotlin.entity

import project.kotlin.security.entity.JwtUser
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class User (
    open var userName: String? = null,
    open var email: String? = null,
    open var password: String? = null,
    open var firstName: String? = null,
    open var lastName: String? = null,
    open var contact: String? = null
)

{
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToOne
    var jwtUser: JwtUser? = null
}

