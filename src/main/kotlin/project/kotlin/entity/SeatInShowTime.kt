package project.kotlin.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id


@Entity
data class SeatInShowTime(var status: Boolean = false,
                          @Column(name = "SeatInShowTime_row")
                          var row: String? = null,
                          @Column(name = "SeatInShowTime_column")
                          var column: String? = null) {
    @Id
    @GeneratedValue
    var id:Long? = null
}
