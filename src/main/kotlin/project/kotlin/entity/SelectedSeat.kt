package project.kotlin.entity

import javax.persistence.*

@Entity
data class SelectedSeat( @ManyToOne
                         var seatDetail: Seat? = null) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var seats =  mutableListOf<SeatInShowTime>()

}