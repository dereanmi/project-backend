package project.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Subtitle(var subtitle:String){

    @Id
    @GeneratedValue
    var id:Long? = null
}