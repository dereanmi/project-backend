package project.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import project.kotlin.service.AmazonClient

@RestController
class BucketController{
    @Autowired
    lateinit var amazonClient: AmazonClient

    @PostMapping("/uploadFile")
    fun uploadFile (@RequestPart("file")file:MultipartFile):ResponseEntity<*>{
        return ResponseEntity.ok(this.amazonClient.uploadFile(file))
    }

    @DeleteMapping("/deleteFile")
    fun deleteFile(@RequestPart("url") fileUrl:String): ResponseEntity<*>{
        return ResponseEntity.ok(this.amazonClient.deleteFileFromS3Bucket(fileUrl))
    }
}