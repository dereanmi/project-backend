package project.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import project.kotlin.service.SubtitleService
import project.kotlin.util.MapperUtil

@RestController
class SubtitleController{
    @Autowired
    lateinit var subtitleService: SubtitleService

    @GetMapping("/subtitle")
    fun getAllSubtitle(): ResponseEntity<Any> {
        val subtitles = subtitleService.getSubtitles()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSubtitleDto(subtitles))
    }
}