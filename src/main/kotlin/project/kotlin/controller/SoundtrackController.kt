package project.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import project.kotlin.entity.Soundtrack
import project.kotlin.service.SoundtrackService
import project.kotlin.util.MapperUtil

@RestController
class SoundtrackController{
    @Autowired
    lateinit var soundtrackService:SoundtrackService

    @GetMapping("/soundtrack")
    fun getAllSoundtrack(): ResponseEntity<Any> {
        val soundtracks = soundtrackService.getSoundtracks()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSoundtrackDto(soundtracks))
    }
}