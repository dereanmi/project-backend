package project.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import project.kotlin.entity.Customer
import project.kotlin.entity.Dto.CustomerDto
import project.kotlin.service.AmazonClient
import project.kotlin.service.CustomerService
import project.kotlin.util.MapperUtil

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService

    @GetMapping("/customers")
    fun getAllCustomers(): ResponseEntity<Any> {
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }

    @GetMapping("/customer/userName")
    fun getCustomerByuserName(@RequestParam("userName") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByuserName(name))
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/firstName")
    fun getCustomerByfirstName(@RequestParam("firstName") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByfirstName(name))
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/lastName")
    fun getCustomerBylastName(@RequestParam("lastName") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerBylastName(name))
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer")
    fun addCustomer(@RequestBody customer:Customer): ResponseEntity<Any> {
        val output = customerService.saveRegister(customer)
        val outputDto = MapperUtil.INSTANCE.mapUserRegisterDto(output)
        return ResponseEntity.ok(output)
    }

    @PutMapping("/customer/{customerId}")
    fun updateCustomer(@PathVariable("customerId") id: Long?,
                       @RequestBody customer: CustomerDto): ResponseEntity<Any> {
        customer.id = id
        var output = customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customer))
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customerService.save(output)))
    }

    @Autowired
    lateinit var amazonClient: AmazonClient

    @PostMapping("/customer/image/{customerId}")
    fun uploadFile(@RequestPart("file") file: MultipartFile,
                   @PathVariable("userId")id : Long): ResponseEntity<*>{
        var imageUrl = this.amazonClient.uploadFile(file)
        var userId = customerService.saveImage(id,imageUrl)
        var output = MapperUtil.INSTANCE.mapCustomerImageDto(userId)
        return ResponseEntity.ok(output)
    }

}