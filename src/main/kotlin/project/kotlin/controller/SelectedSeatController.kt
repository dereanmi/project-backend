package project.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import project.kotlin.service.SelectedSeatService
import project.kotlin.util.MapperUtil

@RestController
class SelectedSeatController{
    @Autowired
    lateinit var selectedSeatService: SelectedSeatService
    @GetMapping("/selectedSeat")
    fun getAllSelectedSeat(): ResponseEntity<Any> {
        val selectedSeats = selectedSeatService.getSelectedSeats()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSelectedSeatDto(selectedSeats))
    }

//    @GetMapping("/selectedSeat/vertical")
//    fun getSelectedSeatByVertical(@RequestParam("vertical") vertical: String): ResponseEntity<Any> {
//        var output = MapperUtil.INSTANCE.mapSelectedSeatDto(selectedSeatService.getSelectedSeatByVertical(vertical))
//        output?.let { return ResponseEntity.ok(output) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }
//
//    @GetMapping("/selectedSeat/horizontal")
//    fun getSelectedSeatByHorizental(@RequestParam("horizontal") horizontal: String): ResponseEntity<Any> {
//        var output = MapperUtil.INSTANCE.mapSelectedSeatDto(selectedSeatService.getSelectedSeatByHorizontal(horizontal))
//        output?.let { return ResponseEntity.ok(output) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }

//    @GetMapping("/selectedSeat/line")
//    fun getSelectedProductByVerticalAndHorizontal(@RequestParam("vertical") vertical: String,
//                                                  @RequestParam("horizontal", required = false) horizontal: String?):ResponseEntity<Any>{
//        val output = MapperUtil.INSTANCE.mapSelectedSeatDto(selectedSeatService.getSelectedSeatByVerticalAndHorizontal(vertical,horizontal))
//        output?.let { return ResponseEntity.ok(output) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }



}