package project.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import project.kotlin.entity.Booking
import project.kotlin.service.BookingService

@RestController
class BookingController{
    @Autowired
    lateinit var bookingService: BookingService

    @PostMapping("/booking")
    fun addBooking(@RequestBody booking: Booking): ResponseEntity<Any> {
        val bookings = bookingService.save(booking)
        return ResponseEntity.ok(bookings)
    }
}
