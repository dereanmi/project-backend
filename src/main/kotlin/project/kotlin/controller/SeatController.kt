package project.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import project.kotlin.service.CinemaService
import project.kotlin.service.SeatService
import project.kotlin.util.MapperUtil

@RestController
class SeatController{
    @Autowired
    lateinit var seatService: SeatService
    @GetMapping("/seat")
    fun getAllSeat(): ResponseEntity<Any> {
        val seats = seatService.getSeats()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSeatDto(seats))
    }

}