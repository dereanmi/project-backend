package project.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import project.kotlin.entity.Cinema
import project.kotlin.service.CinemaService
import project.kotlin.util.MapperUtil

@RestController
class CinemaController{
    @Autowired
    lateinit var cinemaService: CinemaService

    @GetMapping("/cinema")
    fun getAllCinema(): ResponseEntity<Any> {
        val cinemas = cinemaService.getCinemas()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCinemaDto(cinemas))
    }

    @GetMapping("/cinema/name")
    fun getCinemaByName(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCinemaDto(cinemaService.getCinemaByName(name))
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()

    }
}