package project.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import project.kotlin.service.MovieService
import project.kotlin.util.MapperUtil

@RestController
class MovieController{
    @Autowired
    lateinit var moviesService: MovieService
    @GetMapping("/movies")
    fun getAllMovies():ResponseEntity<Any>{
        val movies = moviesService.getMovies()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapMovieDto(movies))
    }

    @GetMapping("/movie/movieName")
    fun getMovieBymovieName(@RequestParam("movieName") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapMovieDto(moviesService.getMovieByMovieName(name))
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

}