package project.kotlin.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.kotlin.entity.Subtitle
import project.kotlin.repository.SubtitleRepository

@Profile("db")
@Repository
class SubtitleDaoImpl : SubtitleDao {

    @Autowired
    lateinit var subtitleRepository: SubtitleRepository

    override fun getSubtitles(): List<Subtitle> {
        return subtitleRepository.findAll().filterIsInstance(Subtitle::class.java)
    }
}