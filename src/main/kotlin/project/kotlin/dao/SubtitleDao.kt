package project.kotlin.dao

import project.kotlin.entity.Subtitle

interface SubtitleDao{
    fun getSubtitles():List<Subtitle>
}