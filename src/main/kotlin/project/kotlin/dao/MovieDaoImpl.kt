package project.kotlin.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.kotlin.entity.Movie
import project.kotlin.repository.MovieRepository

@Profile("db")
@Repository
class MovieDaoImpl : MovieDao {
    override fun getMovieByMovieName(name: String): List<Movie> {
        return movieRepository.findBymovieNameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var movieRepository: MovieRepository

    override fun getMovies(): List<Movie> {
       return movieRepository.findAll().filterIsInstance(Movie::class.java)
    }
}