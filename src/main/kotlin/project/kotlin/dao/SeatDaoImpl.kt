package project.kotlin.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.kotlin.entity.Seat
import project.kotlin.repository.SeatRepository

@Profile("db")
@Repository
class SeatDaoImpl : SeatDao {

    @Autowired
    lateinit var seatRepository: SeatRepository

    override fun getSeats(): List<Seat> {
        return seatRepository.findAll().filterIsInstance(Seat::class.java)
    }
}