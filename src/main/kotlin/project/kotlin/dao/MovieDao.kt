package project.kotlin.dao

import project.kotlin.entity.Movie

interface MovieDao{
    fun getMovies(): List<Movie>
    fun getMovieByMovieName(name: String): List<Movie>
}