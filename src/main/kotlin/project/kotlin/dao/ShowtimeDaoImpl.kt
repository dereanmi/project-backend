package project.kotlin.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository
import project.kotlin.entity.Showtime
import project.kotlin.repository.ShowtimeRepository

@Profile("db")
@Repository
class ShowtimeDaoImpl : ShowtimeDao {
//    override fun getAllShowtimeWithPage(page: Int, pageSize: Int): Page<Showtime> {
//        return showtimeRepository.findAll(PageRequest.of(page,pageSize))
//    }

    override fun findById(showTimeId: Long?): Showtime {
        return showtimeRepository.findById(showTimeId!!).orElse(null)
    }

    override fun getShowtimeByMoviename(name: String): List<Showtime> {
        return showtimeRepository.findBymovies_MovieNameContainingIgnoreCase(name)
    }

    override fun getShowtimeByCinemaname(name: String): List<Showtime> {
        return showtimeRepository.findBycinema_nameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository

    override fun getShowtimes(): List<Showtime> {
        return showtimeRepository.findAll().filterIsInstance(Showtime::class.java)
    }

}