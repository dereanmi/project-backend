package project.kotlin.dao

import project.kotlin.entity.Movie
import project.kotlin.entity.Soundtrack

interface SoundtrackDao{
    fun getSoundtracks(): List<Soundtrack>
}
