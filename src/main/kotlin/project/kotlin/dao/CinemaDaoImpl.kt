package project.kotlin.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.kotlin.entity.Cinema
import project.kotlin.repository.CinemaRepository


@Profile("db")
@Repository
class CinemaDaoImpl: CinemaDao {
    override fun getCinemaByName(name: String): List<Cinema> {
        return cinemaRepository.findBynameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var cinemaRepository: CinemaRepository

    override fun getCinemas(): List<Cinema> {
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }


}