package project.kotlin.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.kotlin.entity.SelectedSeat
import project.kotlin.repository.SelectedSeatRepository

@Profile("db")
@Repository
class SelectedSeatDaoImpl : SelectedSeatDao {
//    override fun getSelectedSeatByVerticalAndHorizontal(vertical: String, horizontal: String?): List<SelectedSeat> {
//        return selectedSeatRepository.findByverticalContainingIgnoreCaseOrhorizontalContainingIgnoreCase(vertical,horizontal)
//    }

//    override fun getSelectedSeatByHorizontal(horizontal: String): List<SelectedSeat> {
//        return selectedSeatRepository.findByhorizontalContainingIgnoreCase(horizontal)
//    }
//
//    override fun getSelectedSeatByVertical(vertical: String): List<SelectedSeat> {
//        return selectedSeatRepository.findByverticalContainingIgnoreCase(vertical)
//    }

    @Autowired
    lateinit var selectedSeatRepository: SelectedSeatRepository

    override fun getSelectedSeats(): List<SelectedSeat> {
        return selectedSeatRepository.findAll().filterIsInstance(SelectedSeat::class.java)
    }
}