package project.kotlin.dao

import project.kotlin.entity.Seat

interface SeatDao{
    fun getSeats(): List<Seat>
}