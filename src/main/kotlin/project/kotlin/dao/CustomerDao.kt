package project.kotlin.dao

import project.kotlin.entity.Customer

interface CustomerDao{
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name: String): List<Customer>
    fun getCustomerByfirstname(name: String): List<Customer>
    fun getCustomerBylastname(name: String): List<Customer>
    fun save(customer: Customer): Customer
    fun saveRegister(user: Customer): Customer
    fun findById(id: Long?): Customer

}