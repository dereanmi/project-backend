package project.kotlin.dao

import project.kotlin.entity.Cinema

interface CinemaDao{
    fun getCinemas(): List<Cinema>
    fun getCinemaByName(name: String): List<Cinema>
}