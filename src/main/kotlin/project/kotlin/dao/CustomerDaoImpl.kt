package project.kotlin.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository
import project.kotlin.entity.Customer
import project.kotlin.repository.CustomerRepository
import project.kotlin.security.entity.Authority
import project.kotlin.security.entity.AuthorityName
import project.kotlin.security.entity.JwtUser
import project.kotlin.security.repository.AuthorityRepository
import project.kotlin.security.repository.UserRepository

@Profile ("db")
@Repository
class CustomerDaoImpl: CustomerDao {
    override fun findById(id: Long?): Customer {
        return customerRepository.findById(id!!).orElse(null)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    override fun save(customer: Customer): Customer {
        return customerRepository.save(customer)
    }


    override fun saveRegister(customer: Customer): Customer {
        val roleUser = Authority(name = AuthorityName.ROLE_CUSTOMER)
        authorityRepository.save(roleUser)
        val encoder = BCryptPasswordEncoder()
        val cust1 = customer
        val custJwt = JwtUser(
                username = cust1.userName,
                password = encoder.encode(cust1.password),
                email = cust1.email,
                enabled = true,
                firstname = cust1.firstName,
                lastname = cust1.lastName
        )
        customerRepository.save(cust1)
        userRepository.save(custJwt)
        cust1.jwtUser = custJwt
        custJwt.authorities.add(roleUser)
        return customerRepository.save(customer)
    }

    override fun getCustomerBylastname(name: String): List<Customer> {
        return  customerRepository.findBylastNameContainingIgnoreCase(name)
    }

    override fun getCustomerByfirstname(name: String): List<Customer> {
        return customerRepository.findByfirstNameContainingIgnoreCase(name)
    }

    override fun getCustomerByName(name: String): List<Customer> {
        return customerRepository.findByuserNameContainingIgnoreCase(name)
    }

    override fun getCustomers(): List<Customer> {
        return customerRepository.findAll().filterIsInstance(Customer::class.java)
    }


}