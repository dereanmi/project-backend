package project.kotlin.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.kotlin.entity.Soundtrack
import project.kotlin.repository.SoundtrackRepository

@Profile("db")
@Repository
class SoundtrackDaoImpl : SoundtrackDao {

    @Autowired
    lateinit var soundtrackReposity: SoundtrackRepository

    override fun getSoundtracks(): List<Soundtrack> {
        return soundtrackReposity.findAll().filterIsInstance(Soundtrack::class.java)
    }
}