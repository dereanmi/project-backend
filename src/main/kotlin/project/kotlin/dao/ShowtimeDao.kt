package project.kotlin.dao

import org.springframework.data.domain.Page
import project.kotlin.entity.Showtime

interface ShowtimeDao{
    fun getShowtimes(): List<Showtime>
    fun getShowtimeByCinemaname(name: String): List<Showtime>
    fun getShowtimeByMoviename(name: String): List<Showtime>
    fun findById(showTimeId: Long?): Showtime
   // fun getAllShowtimeWithPage(page: Int, pageSize: Int): Page<Showtime>
}