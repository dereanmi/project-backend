package project.kotlin.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.kotlin.repository.BookingRepository

@Profile("db")
@Repository
class BookingDaoDBImpl:BookingDao {
    @Autowired
    lateinit var bookingRepository: BookingRepository
}
