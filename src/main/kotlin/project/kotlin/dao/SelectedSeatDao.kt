package project.kotlin.dao

import project.kotlin.entity.SelectedSeat

interface SelectedSeatDao{
    fun getSelectedSeats(): List<SelectedSeat>
//    fun getSelectedSeatByVertical(vertical: String): List<SelectedSeat>
//    fun getSelectedSeatByHorizontal(horizontal: String): List<SelectedSeat>
    //fun getSelectedSeatByVerticalAndHorizontal(vertical: String, horizontal: String?): List<SelectedSeat>
}