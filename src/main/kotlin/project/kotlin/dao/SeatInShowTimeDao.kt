package project.kotlin.dao

import project.kotlin.entity.SeatInShowTime

interface SeatInShowTimeDao {
    fun findById(id: Long?): SeatInShowTime
    fun save(seat: SeatInShowTime): SeatInShowTime

}