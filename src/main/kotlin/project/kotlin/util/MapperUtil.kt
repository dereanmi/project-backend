package project.kotlin.util

import org.mapstruct.InheritConfiguration
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers
import project.kotlin.entity.*
import project.kotlin.entity.Dto.*
import project.kotlin.security.entity.Authority

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapMovieDto(movie: Movie?): MovieDto?
    fun mapMovieDto(movies: List<Movie>): List<Movie>

    fun mapUser(customer: Customer):UserDto

    fun mapCustomerDto(customer: Customer): CustomerDto
    fun mapCustomerDto(customers: List<Customer>): List<CustomerDto>
    fun mapCustomerImageDto(customer: Customer): CustomerImageDto

    fun mapAuthority(authority: Authority): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>

    fun mapSoundtrackDto(soundtrack: Soundtrack?): SoundtrackDto?
    fun mapSoundtrackDto(soundtracks: List<Soundtrack>): List<Soundtrack>

    fun mapSubtitleDto(subtitle: Subtitle?): SubtitleDto?
    fun mapSubtitleDto(subtitles: List<Subtitle>): List<Subtitle>

    fun mapCinemaDto(cinema: Cinema?): CinemaDto?
    fun mapCinemaDto(cinemas: List<Cinema>): List<Cinema>

    fun mapSeatDto(seat: Seat?): SeatDto?
    fun mapSeatDto(seats: List<Seat>): List<Seat>

    fun mapShowtimeDto(showtime: Showtime?): ShowtimeDto?
    fun mapShowtimeDto(showtimes: List<Showtime>): List<Showtime>

    fun mapSelectedSeatDto(selectedSeat: SelectedSeat?): SelectedSeatDto?
    fun mapSelectedSeatDto(selectedSeat: List<SelectedSeat>): List<SelectedSeat>

    fun mapUserRegisterDto(customer: Customer):UserRegisterDto

    @InheritInverseConfiguration
    fun mapCustomerDto(customer:CustomerDto): Customer

    @InheritConfiguration
    fun mapUserRegisterDto(userRegisterDto: UserRegisterDto):Customer




}