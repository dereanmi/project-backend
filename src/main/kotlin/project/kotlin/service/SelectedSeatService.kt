package project.kotlin.service

import project.kotlin.entity.SelectedSeat

interface SelectedSeatService{
    fun getSelectedSeats(): List<SelectedSeat>
//    fun getSelectedSeatByVertical(vertical: String): List<SelectedSeat>
//    fun getSelectedSeatByHorizontal(horizontal: String): List<SelectedSeat>
//    fun getSelectedSeatByVerticalAndHorizontal(vertical: String, horizontal: String?): List<SelectedSeat>

}