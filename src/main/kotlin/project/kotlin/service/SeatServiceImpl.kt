package project.kotlin.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.kotlin.dao.SeatDao
import project.kotlin.entity.Seat

@Service
class SeatServiceImpl:SeatService{
    @Autowired
    lateinit var seatDao: SeatDao

    override fun getSeats(): List<Seat> {
        return seatDao.getSeats()
    }
}