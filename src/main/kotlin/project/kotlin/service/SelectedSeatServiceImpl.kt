package project.kotlin.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.kotlin.dao.SelectedSeatDao
import project.kotlin.entity.SelectedSeat

@Service
class SelectedSeatServiceImpl:SelectedSeatService{
//    override fun getSelectedSeatByVerticalAndHorizontal(vertical: String, horizontal: String?): List<SelectedSeat> {
////        return selectedSeatDao.getSelectedSeatByVerticalAndHorizontal(vertical,horizontal)
////    }

//    override fun getSelectedSeatByHorizontal(horizontal: String): List<SelectedSeat> {
//        return selectedSeatDao.getSelectedSeatByHorizontal(horizontal)
//    }
//
//    override fun getSelectedSeatByVertical(vertical: String): List<SelectedSeat> {
//        return selectedSeatDao.getSelectedSeatByVertical(vertical)
//    }

    @Autowired
    lateinit var selectedSeatDao: SelectedSeatDao

    override fun getSelectedSeats(): List<SelectedSeat> {
        return selectedSeatDao.getSelectedSeats()
    }
}