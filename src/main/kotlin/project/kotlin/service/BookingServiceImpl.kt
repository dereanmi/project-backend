package project.kotlin.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.kotlin.dao.BookingDao
import project.kotlin.dao.SeatInShowTimeDao
import project.kotlin.dao.ShowtimeDao
import project.kotlin.entity.Booking
import project.kotlin.entity.SeatInShowTime

@Service
class BookingServiceImpl: BookingService {
    override fun save(booking: Booking): Booking {
        var showTimeId = showTimeDao.findById(booking.showTimeId)
        var seatMap = mutableListOf<SeatInShowTime>()
        for( item in booking.seats) {
            var seat = seatInShowTimeDao.findById(item.id)
            seat.status = true
            seatMap.add(seatInShowTimeDao.save(seat))
        }
        var book = Booking()
        book.showTimeId = showTimeId.id
        book.seats = seatMap
        book.createdDateTime = System.currentTimeMillis()
        return book
    }

    @Autowired
    lateinit var seatInShowTimeDao: SeatInShowTimeDao

    @Autowired
    lateinit var showTimeDao: ShowtimeDao

    @Autowired
    lateinit var bookingDao: BookingDao
}
