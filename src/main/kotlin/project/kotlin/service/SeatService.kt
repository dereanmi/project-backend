package project.kotlin.service

import project.kotlin.entity.Seat

interface SeatService {
    fun getSeats(): List<Seat>
}