package project.kotlin.service

import project.kotlin.entity.Cinema
import project.kotlin.entity.Customer

interface CinemaService {
    fun getCinemas(): List<Cinema>
    fun getCinemaByName(name: String): List<Cinema>
}