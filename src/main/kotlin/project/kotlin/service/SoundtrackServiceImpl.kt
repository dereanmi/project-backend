package project.kotlin.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.kotlin.dao.SoundtrackDao
import project.kotlin.entity.Soundtrack

@Service
class SoundtrackServiceImpl:SoundtrackService{
    @Autowired
    lateinit var soundtrackDao: SoundtrackDao

    override fun getSoundtracks(): List<Soundtrack> {
        return soundtrackDao.getSoundtracks()
    }
}