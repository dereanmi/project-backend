package project.kotlin.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.kotlin.dao.CinemaDao
import project.kotlin.entity.Cinema

@Service
class CinemaServiceImpll: CinemaService{
    override fun getCinemaByName(name: String): List<Cinema> {
        return cinemaDao.getCinemaByName(name)
    }

    @Autowired
    lateinit var cinemaDao: CinemaDao

    override fun getCinemas(): List<Cinema> {
        return cinemaDao.getCinemas()
    }
}