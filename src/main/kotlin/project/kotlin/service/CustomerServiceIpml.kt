package project.kotlin.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.kotlin.dao.CustomerDao
import project.kotlin.entity.Customer
import project.kotlin.entity.Dto.CustomerDto
import project.kotlin.util.MapperUtil

@Service
class CustomerServiceIpml: CustomerService{

    override fun saveRegister(userRegisterDto: Customer): Customer {
        return customerDao.saveRegister(userRegisterDto)
    }

    override fun save(customer: Customer): Customer {
        return customerDao.save(customer)
    }



    override fun saveImage(id: Long, imageUrl: String): Customer {
        val user = customerDao.findById(id)
        user.imageUrl = imageUrl
        return customerDao.save(user)
    }

    override fun getCustomerBylastName(name: String): List<Customer> {
        return customerDao.getCustomerBylastname(name)
1    }

    override fun getCustomerByfirstName(name: String): List<Customer> {
        return customerDao.getCustomerByfirstname(name)
    }


    override fun getCustomerByuserName(name: String): List<Customer> {
        return customerDao.getCustomerByName(name)
    }

    @Autowired
    lateinit var customerDao : CustomerDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }
}