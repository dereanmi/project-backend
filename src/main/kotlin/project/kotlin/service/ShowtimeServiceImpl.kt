package project.kotlin.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import project.kotlin.dao.ShowtimeDao
import project.kotlin.entity.Showtime
import javax.transaction.Transactional

@Service
class ShowtimeServiceImpl:ShowtimeService{

//    @Transactional
//    override fun getAllShowTimeWithPage(page: Int, pageSize: Int): Page<Showtime> {
//        return showtimeDao.getAllShowtimeWithPage(page,pageSize)
//    }


    override fun getShowtimeByMovieName(name: String): List<Showtime> {
        return showtimeDao.getShowtimeByMoviename(name)
    }

    override fun getShowtimeByCinemaName(name: String): List<Showtime> {
        return showtimeDao.getShowtimeByCinemaname(name)
    }

    @Autowired
    lateinit var showtimeDao: ShowtimeDao

    override fun getShowtimes(): List<Showtime> {
        return showtimeDao.getShowtimes()
    }

}