package project.kotlin.service

import project.kotlin.entity.Booking

interface BookingService{
    fun save(booking: Booking): Booking
}
