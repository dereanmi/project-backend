package project.kotlin.service

import org.springframework.data.domain.Page
import project.kotlin.entity.Customer
import project.kotlin.entity.Dto.CustomerDto

interface CustomerService {

    fun getCustomers(): List<Customer>
    fun getCustomerByuserName(name: String): List<Customer>
    fun getCustomerByfirstName(name: String): List<Customer>
    fun getCustomerBylastName(name: String): List<Customer>
    fun save(customer: Customer): Customer
    fun saveImage(id: Long, imageUrl: String): Customer
    abstract fun saveRegister(userRegisterDto: Customer): Customer

}