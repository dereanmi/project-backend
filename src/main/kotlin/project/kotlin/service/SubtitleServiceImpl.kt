package project.kotlin.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.kotlin.dao.SubtitleDao
import project.kotlin.entity.Subtitle

@Service
class SubtitleServiceImpl:SubtitleService{
    @Autowired
    lateinit var subtitleDao: SubtitleDao

    override fun getSubtitles(): List<Subtitle> {
        return subtitleDao.getSubtitles()
    }
}