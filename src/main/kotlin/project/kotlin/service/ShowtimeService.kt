package project.kotlin.service

import org.springframework.data.domain.Page
import project.kotlin.entity.Showtime

interface ShowtimeService {
    fun getShowtimes(): List<Showtime>
    fun getShowtimeByCinemaName(name: String): List<Showtime>
    fun getShowtimeByMovieName(name: String): List<Showtime>
//    fun getAllShowTimeWithPage(page: Int, pageSize: Int):Page<Showtime>
}