package project.kotlin.service

import project.kotlin.entity.Soundtrack

interface SoundtrackService{
    fun getSoundtracks():List<Soundtrack>
}