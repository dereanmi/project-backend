package project.kotlin.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.kotlin.dao.MovieDao
import project.kotlin.entity.Movie

@Service
class MovieServiceImpl:MovieService{
    override fun getMovieByMovieName(name: String): List<Movie> {
        return moviesDao.getMovieByMovieName(name)
    }

    @Autowired
    lateinit var moviesDao: MovieDao
    override fun getMovies(): List<Movie> {
        return moviesDao.getMovies()
    }
}