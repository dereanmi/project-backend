package project.kotlin.service

import project.kotlin.entity.Subtitle

interface SubtitleService{
    fun getSubtitles(): List<Subtitle>
}