package project.kotlin.service

import project.kotlin.entity.Movie

interface MovieService{
    fun getMovies():List<Movie>
    fun getMovieByMovieName(name: String): List<Movie>
}