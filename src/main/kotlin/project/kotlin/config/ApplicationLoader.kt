package project.kotlin.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import project.kotlin.entity.*
import project.kotlin.repository.*
import project.kotlin.security.entity.Authority
import project.kotlin.security.entity.AuthorityName
import project.kotlin.security.entity.JwtUser
import project.kotlin.security.repository.AuthorityRepository
import project.kotlin.security.repository.UserRepository
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var movieRepository: MovieRepository
    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var soundtrackRepository: SoundtrackRepository
    @Autowired
    lateinit var subtitleRepository: SubtitleRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    @Autowired
    lateinit var seatRepository: SeatRepository
    @Autowired
    lateinit var selectedSeatRepository: SelectedSeatRepository
    @Autowired
    lateinit var seatInShowTimeRepository: SeatInShowTimeRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val cust1 = Customer("สมชาติ","a@b.com")
        val custJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = cust1.email,
                enabled = true,
                firstname = cust1.userName,
                lastname = "unknown"
        )
        customerRepository.save(cust1)
        userRepository.save(custJwt)
        cust1.jwtUser = custJwt
        custJwt.user = cust1
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)
    }


    @Transactional
    override fun run(args: ApplicationArguments?) {

        val movie1 = movieRepository.save(Movie("อลิตา แบทเทิล แองเจิ้ล",125,"https://lh3.googleusercontent.com/m-KIGZSYMapNnCIGB3QVXPlqkv7Vu_-VoEXqdleoNZ-CDRVdprMcoUanKbMHeFjlXk38FYPxW0Gar0XEQYI=w1024"))
        val movie2 = movieRepository.save(Movie("อภินิหารไวกิ้งพิชิตมังกร 3",105,"https://lh3.googleusercontent.com/xTpxPyQOfsnpA2lkp8Fz7LjVA-hsadGF0U7c5rwJq4ikRG_np-5_kO-gzHMVkur2Y5BnLQ4Sfki_Qj7T0Mlisw=w1024"))
        val movie3 = movieRepository.save(Movie("Captain Marvel",130,"https://lh3.googleusercontent.com/3Ost-FvXYMVAs84hLxJAn9qqIh9s_YCC8oCySCuOWIJWIu8zPIuKYJ9FG_FEFBqKZoQnguVOuNAkg9kArkPM=w1024"))
        val movie4 = movieRepository.save(Movie("เฟรนด์โซน ระวัง..สิ้นสุดทางเพื่อน",120,"https://lh3.googleusercontent.com/hGX4-13iqHM1Kx_nyj67AtCsFXAjJyS0wxoEcJynOs99BUJXQWLIUQFcsje3jdMW75wtrJU2ktywEF3gxoKe=w1024"))
        val movie5 = movieRepository.save(Movie("สุขสันต์วันตาย 2U",100,"https://lh3.googleusercontent.com/8hKivfDWOC2G8rQolX9850yiPWs5dJzSgyGirJFjU66jeUPV9-5gkm1soLxlHubI5_V_lK1T0vdfinsUlVZtOA=w1024"))
        val movie6 = movieRepository.save(Movie("คุซามะ อินฟินิตี้",80,"https://lh3.googleusercontent.com/Fs3-9D0HjOaP-BWhOhVp-gZAqN3aRyyB5Z7W59f67v_frABpaljFUhFR9Pjs-fTN37jz1mqml9LRUK1tVjjFBg=w1024"))
        val movie7 = movieRepository.save(Movie("คนเหนือมนุษย์",130,"https://lh3.googleusercontent.com/rVCTrkvoC4Iwpv4sPY8gSGYom0rKm28exIaVBD2sGYUip0SMuKqfDxyAVrd_Ps_H39Ss-NGT1nzytQPuBunr=w1024"))


        val customer1 = customerRepository.save(Customer("Dream","dereanmi@gmail.com","12345678","Thanchanok","Phrompalit","0932191666"))
        val customer2 = customerRepository.save(Customer("Prawfon","Prawfon@hotmai.com","003136458","Prawfon","Thitimatiyakul","0956983438"))

        val soundtrack1 = soundtrackRepository.save(Soundtrack("TH"))
        val soundtrack2 = soundtrackRepository.save(Soundtrack("EN"))

        val subtitle1 = subtitleRepository.save(Subtitle("TH"))
        val subtitle2 = subtitleRepository.save(Subtitle("EN"))

        val cinema1 = cinemaRepository.save(Cinema("Cinema 1"))
        val cinema2 = cinemaRepository.save(Cinema("Cinema 2"))
        val cinema3 = cinemaRepository.save(Cinema("Cinema 3"))

//        val selectedSeat1 = selectedSeatRepository.save(SelectedSeat(8,13,false))
//        val selectedSeat2 = selectedSeatRepository.save(SelectedSeat(1,3,false))
//        val selectedSeat3 = selectedSeatRepository.save(SelectedSeat(1,2,false))

        var premium = seatRepository.save(Seat("Premium",SeatType.PREMIUM,190.00,4,20))
        var deluxe = seatRepository.save(Seat("Deluxe",SeatType.DELUXE,150.00,10,20))
        var sofa = seatRepository.save(Seat("Sofa",SeatType.SOFA,500.00,1,6))

        cinema1.seat = mutableListOf(sofa,premium,sofa)
        cinema2.seat = mutableListOf(premium,deluxe)
        cinema3.seat = mutableListOf(sofa,premium,deluxe)

//===========================================Seats in Cinema and Showtime 1==========================================================
        val time1 = showtimeRepository.save(Showtime(1553490000000,1553497200000))
        time1.movies = movie1
        time1.cinema = cinema1
        //time1.selectedSeats.add(selectedSeat1)

        var row: Char = 'A'
        for( (index, item) in time1.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.rows!!){
                for(indexC in 1..selectedSeat.seatDetail!!.columns!!){
                    var seat: SeatInShowTime
                    if (item.seatType === SeatType.SOFA){
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    }
                    else{
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            time1.selectedSeats.add(selectedSeat)
            if(item.seatType === SeatType.SOFA){
                row = 'A'
            }
        }

//===========================================Seats in Cinema and Showtime 2==========================================================

        val time2 = showtimeRepository.save(Showtime(1553576400000,1553583600000))
        time2.movies = movie2
        time2.cinema = cinema2
        //time2.selectedSeats.add(selectedSeat2)
        row = 'A'
        for( (index, item) in time2.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.rows!!){
                for(indexC in 1..selectedSeat.seatDetail!!.columns!!){
                    var seat: SeatInShowTime
                    if (item.seatType === SeatType.SOFA){
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    }
                    else{
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            time2.selectedSeats.add(selectedSeat)
            if(item.seatType === SeatType.SOFA){
                row = 'A'
            }
        }


//===========================================Seats in Cinema and Showtime 3==========================================================

        val time3 = showtimeRepository.save(Showtime(1553662800000,1553670000000))
        time3.movies = movie3
        time3.cinema = cinema3
        //time3.selectedSeats.add(selectedSeat3)
        row = 'A'
        for( (index, item) in time3.cinema!!.seat.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.rows!!){
                for(indexC in 1..selectedSeat.seatDetail!!.columns!!){
                    var seat: SeatInShowTime
                    if (item.seatType === SeatType.SOFA){
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    }
                    else{
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            time3.selectedSeats.add(selectedSeat)
            if(item.seatType === SeatType.SOFA){
                row = 'A'
            }
        }



        movie1.soundtrack.add(soundtrack1)
        movie1.soundtrack.add(soundtrack2)
        movie2.soundtrack.add(soundtrack1)
        movie2.soundtrack.add(soundtrack2)
        movie3.soundtrack.add(soundtrack1)
        movie3.soundtrack.add(soundtrack2)
        movie4.soundtrack.add(soundtrack1)
        movie5.soundtrack.add(soundtrack1)
        movie5.soundtrack.add(soundtrack2)
        movie6.soundtrack.add(soundtrack2)
        movie7.soundtrack.add(soundtrack1)
        movie7.soundtrack.add(soundtrack2)


        movie1.subtitle.add(subtitle1)
        movie2.subtitle.add(subtitle1)
        movie3.subtitle.add(subtitle1)
        movie4.subtitle.add(subtitle2)
        movie5.subtitle.add(subtitle1)
        movie6.subtitle.add(subtitle1)
        movie7.subtitle.add(subtitle2)

        loadUsernameAndPassword()
    }
}